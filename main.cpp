#include "main.h"

//check if user enters input file
int checkInputFilePath(const int inputcount, char **inputarray, std::string &filepath){
    if (inputcount < 2)
        return -1;
            
    filepath = inputarray[1];
    return 0;
}

//check if input file exists
bool openFileOnReadMode(const std::string inputfilepath, std::fstream &inputfile){
    inputfile.open(inputfilepath, std::ios::in);
    return inputfile.is_open() ? true : false;
}


int main(int argc, char *argv[]){

    //set page table size
    std::vector<int> frame_size = {64, 128, 256, 512};

    for (int selectedPolicy = 0; selectedPolicy < pageRepacementPolicyCount; selectedPolicy++){

        if (selectedPolicy == 0)
            printf("LFU policy:\n");
        else if(selectedPolicy == 1)
            printf("LRU policy:\n");

        //start to measure page performance 
        struct  timeval start;
        struct  timeval end;
        double elapsedtime = 0;
        gettimeofday(&start,NULL);

        printf("Frame\tHit\t\tMiss\t\tPage fault ratio\n");

        for (int selectedFrameSize = 0; selectedFrameSize < frame_size.size(); selectedFrameSize++){
            std::string inputfilepath;
            if (checkInputFilePath(argc, argv, inputfilepath) == -1){
                printf("Please input a test file.\n");
                return -1;
            }
                

            char inputbuffer[256];
            std::fstream inputfile;

            if (!openFileOnReadMode(inputfilepath, inputfile)){
                printf("File open is failed.\n");
                return -1;
            }
                
            PageTable table(frame_size[selectedFrameSize]);

            //refcounter is the number of current total input page 
            //hit is the number of current total page hit
            //miss is the number of current total page miss
            unsigned long long refcounter = 0, hit = 0, miss = 0;
            
            //read input file until eof
            while (!inputfile.eof()){

                inputfile.getline(inputbuffer, 256, '\n');
                refcounter++;
                std::string pagenum;
                pagenum.assign(inputbuffer);

		        if(!pagenum.empty()){
                     if (table.pageTableRead(stoull(pagenum), refcounter, selectedPolicy))
                         hit++;
                     else
                         miss++;
		        }
            }
            
            //show hit and miss rate
            printf("%d\t%llu\t\t%llu\t\t%.10f\n", frame_size[selectedFrameSize], hit, miss, (double)miss / refcounter);
            inputfile.close();
        }

        //end with measurement of page performance 
        gettimeofday(&end,NULL);
        elapsedtime = (double)(1000000 * (end.tv_sec-start.tv_sec)+ end.tv_usec-start.tv_usec)/1000000;
        printf("Total elapsed time %.4f sec\n\n",elapsedtime);
    }

    return 0;
}
