#include "lib/PageTable.h"

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sys/time.h>

//Set the amount of page replacement algorithm
#define pageRepacementPolicyCount 2