# Page Replacement Simulation
There are two page replacement algorithms.
1. LFU （Least Frequently Used）
2. LRU （Least Recently Used）

# Version
1.0

# Environment
### OS
* Ubuntu 18.04

### Pre-install
* g++
* make

# Building Source Code
Bulid source code by executing the following command.
``` 
make 
```
If you want to rebuild, please do the following command in advance.
``` 
make clean
```

# Run
To do program correctness test, please run the following command.
``` 
./main.out ./testcase/Zipfian_whois/whois.txt
```
The result is as follow.

![Correctness](testcase/result/CorrectnessTest.JPG)

You can run the following command to evaluate program performance by sample input.
``` 
./main.out ./testcase/Zipfian_whois/zipf.txt
```
The result is as follow.

![SamplePerformance](testcase/result/SamplePerformance.JPG)

