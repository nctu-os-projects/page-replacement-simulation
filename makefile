CC:=g++
out:=main.out
src:=main.cpp ./lib/PageNode.cpp ./lib/PageTable.cpp

all: $(exe)
	$(CC) -O3 -o $(out) $(src) 

.PHONY:clean
clean:
	rm -rf $(out)