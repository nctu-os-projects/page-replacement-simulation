#include "PageNode.h"

PageNode::PageNode(unsigned long long in_ref_counter, unsigned long long in_late_ref, unsigned long long in_page_num) : ref_counter(in_ref_counter), late_ref(in_late_ref), page_num(in_page_num) {}
PageNode::PageNode() : ref_counter(0), late_ref(0), page_num(0) {}