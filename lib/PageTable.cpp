#include "PageTable.h"

bool compaLFU(PageNode *first, PageNode *second){
    if (first->ref_counter > second->ref_counter)
        return true;

    else if ((first->ref_counter == second->ref_counter) && (first->late_ref > second->late_ref))
        return true;

    else
        return false;
}

bool compaLRU(PageNode *first, PageNode *second){
    if (first->late_ref > second->late_ref)
        return true;
    else
        return false;
}

void PageTable::insetPageWithoutReplace(unsigned long long in_page_num, unsigned long long ref, int victim_method){
    page_table[in_page_num] = new PageNode(1, ref, in_page_num);
    victim_table.push_back(page_table[in_page_num]);
}

void PageTable::insetPageWithtReplace(unsigned long long in_page_num, unsigned long long ref, int victim_method){

    if (victim_method == 0){

        //check if victim_table is max-heap
        if (!std::is_heap(victim_table.begin(), victim_table.end()))
            std::make_heap(victim_table.begin(), victim_table.end(), compaLFU);
    }

    else{

        if (!std::is_heap(victim_table.begin(), victim_table.end()))
            std::make_heap(victim_table.begin(), victim_table.end(), compaLRU);
    }

    std::pop_heap(victim_table.begin(), victim_table.end());
    unsigned long long oldpagenum = victim_table.back()->page_num;
    delete victim_table.back();
    victim_table.pop_back();
    page_table.erase(oldpagenum);
    page_table[in_page_num] = new PageNode(1, ref, in_page_num);
    victim_table.push_back(page_table[in_page_num]);
}

void PageTable::cleanTable(){

    for (std::unordered_map<unsigned long long, PageNode *>::iterator it = page_table.begin(); it != page_table.end(); it++){
        delete it->second;
    }

    page_table.clear();
    victim_table.clear();
}

PageTable::PageTable(unsigned int in_len) : table_len(in_len){
    page_table.reserve(table_len);
    victim_table.reserve(table_len);
}

void PageTable::flushPageTable(){
    cleanTable();
}

void PageTable::makeNewPageTable(unsigned int in_len){
    table_len = in_len;
    page_table.reserve(table_len);
    victim_table.reserve(table_len);
}

bool PageTable::pageTableRead(unsigned long long in_page_num, unsigned long long int ref, int victim_method){

    if (page_table.find(in_page_num) != page_table.end()){

        (page_table[in_page_num]->ref_counter)++;
        (page_table[in_page_num]->late_ref) = ref;

        return true;
    }
    
    else{

        //detect if the capacity of page table is full
        if ((victim_table.size() + 1) <= table_len)
            insetPageWithoutReplace(in_page_num, ref, victim_method);

        else
            insetPageWithtReplace(in_page_num, ref, victim_method);

        return false;
    }
}

PageTable::~PageTable(){
    cleanTable();
}