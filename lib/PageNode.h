class PageNode{
    friend class PageTable;
    //LFU compared function for the victim_table variable of PageTable class
    friend bool compaLFU(PageNode *, PageNode *);
    //LRU compared function for the victim_table variable of PageTable class
    friend bool compaLRU(PageNode *, PageNode *);

    // total reference amount of the page
    unsigned long long ref_counter;
    // last reference number of the page
    unsigned long long late_ref;
    // page index
    unsigned long long page_num;
    PageNode(unsigned long long in_ref_counter, unsigned long long in_late_ref, unsigned long long in_page_num);
    PageNode();
};

