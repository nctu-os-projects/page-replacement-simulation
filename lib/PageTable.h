#include "PageNode.h"

#include <vector>
#include <string>
#include <algorithm>
#include <unordered_map>

class PageTable{

    unsigned int table_len;

    //For every page, only one PageNode variable is referenced in page_table and victim_table.
    //page_table access a PageNode variable by page index, which is the key of page_table and used in hash function.
    //victim_table is a max-heap whose top is selected victim page.
    std::unordered_map<unsigned long long, PageNode *> page_table;
    std::vector<PageNode *> victim_table;

    //victim_method is 0, LFU method.
    //victim_method is 1, LRU method.
    void insetPageWithoutReplace(unsigned long long in_page_num, unsigned long long ref, int victim_method);
    void insetPageWithtReplace(unsigned long long in_page_num, unsigned long long ref, int victim_method);
    void cleanTable();

public:

    PageTable(unsigned int in_len);

    //flush now page table content
    void flushPageTable();
    //create a new page table with size in_len
    void makeNewPageTable(unsigned int in_len);
    //check if page is at the page table
    //if not, insert page index into the page table
    bool pageTableRead(unsigned long long in_page_num, unsigned long long int ref, int victim_method);
    ~PageTable();

};